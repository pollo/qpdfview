<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="af">
    <extra-po-header-po_revision_date>2022-05-29 15:35+0000</extra-po-header-po_revision_date>
    <extra-po-header-x_launchpad_export_date>2022-05-30 06:46+0000</extra-po-header-x_launchpad_export_date>
    <extra-po-headers>Project-Id-Version,Report-Msgid-Bugs-To,POT-Creation-Date,PO-Revision-Date,Last-Translator,Language-Team,MIME-Version,Content-Type,Content-Transfer-Encoding,X-Launchpad-Export-Date,X-Qt-Contexts</extra-po-headers>
    <extra-po-header-report_msgid_bugs_to>FULL NAME &lt;EMAIL@ADDRESS&gt;</extra-po-header-report_msgid_bugs_to>
    <extra-po-header-language_team>Afrikaans &lt;af@li.org&gt;</extra-po-header-language_team>
    <extra-po-header-project_id_version>qpdfview</extra-po-header-project_id_version>
    <extra-po-header-pot_creation_date>2021-04-23 17:12+0000</extra-po-header-pot_creation_date>
    <extra-po-header_comment># Afrikaans translation for qpdfview
# Copyright (c) 2022 Rosetta Contributors and Canonical Ltd 2022
# This file is distributed under the same license as the qpdfview package.
# FIRST AUTHOR &lt;EMAIL@ADDRESS&gt;, 2022.
#</extra-po-header_comment>
    <extra-po-header-last_translator>Bernard Stafford &lt;Unknown&gt;</extra-po-header-last_translator>
<context>
    <name>Model::ImageDocument</name>
    <message>
        <location filename="../sources/imagemodel.cpp" line="121"/>
        <source>Image (%1)</source>
        <translation>Beeld (%1)</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="149"/>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="150"/>
        <source>Resolution</source>
        <translation>Resolusie</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="151"/>
        <source>Depth</source>
        <translation>Diepte</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="159"/>
        <location filename="../sources/imagemodel.cpp" line="162"/>
        <location filename="../sources/imagemodel.cpp" line="165"/>
        <location filename="../sources/imagemodel.cpp" line="168"/>
        <location filename="../sources/imagemodel.cpp" line="173"/>
        <location filename="../sources/imagemodel.cpp" line="177"/>
        <source>Format</source>
        <translation>Formaat</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="159"/>
        <source>Monochrome</source>
        <translation>Monokroom</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="162"/>
        <source>Indexed</source>
        <translation>Geïndekseer</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="165"/>
        <source>32 bits RGB</source>
        <translation>32 bits RGB</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="168"/>
        <source>32 bits ARGB</source>
        <translation>32 bits ARGB</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="173"/>
        <source>16 bits RGB</source>
        <translation>16 bits RGB</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="177"/>
        <source>24 bits RGB</source>
        <translation>24 bits RGB</translation>
    </message>
</context>
<context>
    <name>Model::PdfDocument</name>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1094"/>
        <source>Linearized</source>
        <translation>Gelineariseerd</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="248"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="250"/>
        <source>Type</source>
        <translation>Tipe</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="252"/>
        <source>Embedded</source>
        <translation>Ingebed</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="254"/>
        <source>Subset</source>
        <translation>Onderversameling</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="256"/>
        <source>File</source>
        <translation>Lêer</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="278"/>
        <location filename="../sources/pdfmodel.cpp" line="280"/>
        <location filename="../sources/pdfmodel.cpp" line="1093"/>
        <location filename="../sources/pdfmodel.cpp" line="1094"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1091"/>
        <source>PDF version</source>
        <translation>PDF weergawe</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1093"/>
        <source>Encrypted</source>
        <translation>Geënkripteer</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="278"/>
        <location filename="../sources/pdfmodel.cpp" line="280"/>
        <location filename="../sources/pdfmodel.cpp" line="1093"/>
        <location filename="../sources/pdfmodel.cpp" line="1094"/>
        <source>No</source>
        <translation>Geen</translation>
    </message>
</context>
<context>
    <name>Model::PdfPage</name>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="767"/>
        <source>Information</source>
        <translation>Inligting</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="767"/>
        <source>Version 0.20.1 or higher of the Poppler library is required to add or remove annotations.</source>
        <translation>Weergawe 0.20.1 of hoër van die Poppler-biblioteek word vereis om aantekeninge by te voeg of te verwyder.</translation>
    </message>
</context>
<context>
    <name>Model::PsDocument</name>
    <message>
        <location filename="../sources/psmodel.cpp" line="239"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="240"/>
        <source>Created for</source>
        <translation>Geskep vir</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="241"/>
        <source>Creator</source>
        <translation>Skepper</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="242"/>
        <source>Creation date</source>
        <translation>Skepping datum</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="243"/>
        <source>Format</source>
        <translation>Formaat</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="244"/>
        <source>Language level</source>
        <translation>Taalvlak</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../sources/main.cpp" line="172"/>
        <source>An empty instance name is not allowed.</source>
        <translation>&apos;n Leë instansienaam word nie toegelaat nie.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="183"/>
        <source>An empty search text is not allowed.</source>
        <translation>&apos;n Leë soekteks is nie toegelaat nie.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="240"/>
        <source>Unknown command-line option &apos;%1&apos;.</source>
        <translation>Onbekende opdragreël opsie &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="271"/>
        <source>Using &apos;--instance&apos; requires an instance name.</source>
        <translation>Gebruik &apos;--instance&apos; vereis &apos;n instansie naam.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="277"/>
        <source>Using &apos;--instance&apos; is not allowed without using &apos;--unique&apos;.</source>
        <translation>Gebruik &apos;--instance&apos; word nie toegelaat sonder om te gebruik &apos;--unique&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="283"/>
        <source>An instance name must only contain the characters &quot;[A-Z][a-z][0-9]_&quot; and must not begin with a digit.</source>
        <translation>&apos;n Instance naam moet slegs bevat die karakters &quot;[A-Z][a-z][0-9]_&quot; en mag nie met &apos;n syfer begin nie.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="289"/>
        <source>Using &apos;--search&apos; requires a search text.</source>
        <translation>Gebruik &apos;--search&apos; vereis &apos;n soekteks.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="449"/>
        <source>Could not prepare signal handler.</source>
        <translation>Kon nie seinhanteerder voorberei nie.</translation>
    </message>
</context>
<context>
    <name>QShortcut</name>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="886"/>
        <source>Shift</source>
        <translation>Verskuiwing</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="887"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="888"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="889"/>
        <source>Shift and Ctrl</source>
        <translation>Verskuiwing en Ctrl</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="890"/>
        <source>Shift and Alt</source>
        <translation>Verskuiwing en Alt</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="891"/>
        <source>Ctrl and Alt</source>
        <translation>Ctrl en Alt</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="892"/>
        <source>Right mouse button</source>
        <translation>Regs muis knoppie</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="893"/>
        <source>Middle mouse button</source>
        <translation>Middelste muis knoppie</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="894"/>
        <source>None</source>
        <translation>Geen</translation>
    </message>
</context>
<context>
    <name>qpdfview::BookmarkDialog</name>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="40"/>
        <source>Bookmark</source>
        <translation>Boekmerk</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="49"/>
        <source>Page:</source>
        <translation>Bladsy:</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="54"/>
        <source>Label:</source>
        <translation>Etiket:</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="59"/>
        <source>Comment:</source>
        <translation>Kommentaar:</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="65"/>
        <source>Modified:</source>
        <translation>Gewysig:</translation>
    </message>
</context>
<context>
    <name>qpdfview::BookmarkMenu</name>
    <message>
        <location filename="../sources/bookmarkmenu.cpp" line="42"/>
        <source>&amp;Open</source>
        <translation>&amp;Oop</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkmenu.cpp" line="46"/>
        <source>Open in new &amp;tab</source>
        <translation>Maak oop in nuwe &amp;oortjie</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkmenu.cpp" line="55"/>
        <source>&amp;Remove bookmark</source>
        <translation>&amp;Verwyder boekmerk</translation>
    </message>
</context>
<context>
    <name>qpdfview::Database</name>
    <message>
        <location filename="../sources/database.cpp" line="993"/>
        <source>Jump to page %1</source>
        <translation>Spring na bladsy %1</translation>
    </message>
</context>
<context>
    <name>qpdfview::DocumentView</name>
    <message>
        <location filename="../sources/documentview.cpp" line="1446"/>
        <location filename="../sources/documentview.cpp" line="2084"/>
        <source>Information</source>
        <translation>Inligting</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="1446"/>
        <source>The source editor has not been set.</source>
        <translation>Die bron redigeerder is nie gestel nie.</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2084"/>
        <source>Opening URL is disabled in the settings.</source>
        <translation>Opening URL is gedeaktiveer in die instellings.</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2124"/>
        <source>Warning</source>
        <translation>Waarskuwing</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2124"/>
        <location filename="../sources/main.cpp" line="365"/>
        <source>SyncTeX data for &apos;%1&apos; could not be found.</source>
        <translation>SyncTeX data vir &apos;%1&apos; kon nie gevind word nie.</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2643"/>
        <source>Printing &apos;%1&apos;...</source>
        <translation>Drukwerk &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2717"/>
        <source>Unlock %1</source>
        <translation>Ontsluit %1</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2717"/>
        <source>Password:</source>
        <translation>Wagwoord:</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="538"/>
        <source>Page %1</source>
        <translation>Bladsy %1</translation>
    </message>
</context>
<context>
    <name>qpdfview::FileAttachmentAnnotationWidget</name>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="116"/>
        <source>Save...</source>
        <translation>Stoor...</translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="117"/>
        <source>Save and open...</source>
        <translation>Stoor en maak oop...</translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="164"/>
        <source>Save file attachment</source>
        <translation>Stoor lêer aanhegsel</translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="180"/>
        <location filename="../sources/annotationwidgets.cpp" line="186"/>
        <source>Warning</source>
        <translation>Waarskuwing</translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="180"/>
        <source>Could not open file attachment saved to &apos;%1&apos;.</source>
        <translation>Kon nie oop lêer-aanhegsel gestoorde na &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="186"/>
        <source>Could not save file attachment to &apos;%1&apos;.</source>
        <translation>Kon nie lêer aanhegting stoor na &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>qpdfview::FontsDialog</name>
    <message>
        <location filename="../sources/fontsdialog.cpp" line="37"/>
        <source>Fonts</source>
        <translation>Lettertipes</translation>
    </message>
</context>
<context>
    <name>qpdfview::HelpDialog</name>
    <message>
        <location filename="../sources/helpdialog.cpp" line="41"/>
        <source>Help</source>
        <translation>Hulp</translation>
    </message>
    <message>
        <location filename="../sources/helpdialog.cpp" line="48"/>
        <source>help.html</source>
        <extracomment>Please replace by file name of localized help if available, e.g. &quot;help_fr.html&quot;.
</extracomment>
        <translation>help.html</translation>
    </message>
    <message>
        <location filename="../sources/helpdialog.cpp" line="63"/>
        <source>Find previous</source>
        <translation>Vind vorige</translation>
    </message>
    <message>
        <location filename="../sources/helpdialog.cpp" line="67"/>
        <source>Find next</source>
        <translation>Vind volgende</translation>
    </message>
</context>
<context>
    <name>qpdfview::MainWindow</name>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3309"/>
        <source>Toggle tool bars</source>
        <translation>Wissel nutsbalke</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3310"/>
        <source>Toggle menu bar</source>
        <translation>Wissel spyskaartbalk</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="484"/>
        <location filename="../sources/mainwindow.cpp" line="526"/>
        <location filename="../sources/mainwindow.cpp" line="1487"/>
        <location filename="../sources/mainwindow.cpp" line="1498"/>
        <location filename="../sources/mainwindow.cpp" line="1504"/>
        <location filename="../sources/mainwindow.cpp" line="1520"/>
        <location filename="../sources/mainwindow.cpp" line="1540"/>
        <location filename="../sources/mainwindow.cpp" line="1578"/>
        <location filename="../sources/mainwindow.cpp" line="1719"/>
        <location filename="../sources/mainwindow.cpp" line="2813"/>
        <location filename="../sources/mainwindow.cpp" line="2823"/>
        <source>Warning</source>
        <translation>Waarskuwing</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="484"/>
        <location filename="../sources/mainwindow.cpp" line="526"/>
        <source>Could not open &apos;%1&apos;.</source>
        <translation>Kon nie oopmaak &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="798"/>
        <source>Copy file path</source>
        <translation>Kopieer lêerpad</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="799"/>
        <source>Select file path</source>
        <translation>Selekteer lêerpad</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="801"/>
        <source>Close all tabs</source>
        <translation>Sluit alle oortjies</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="802"/>
        <source>Close all tabs but this one</source>
        <translation>Sluit alle oortjies behalwe hierdie een</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="803"/>
        <source>Close all tabs to the left</source>
        <translation>Maak alle oortjies om links toe</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="804"/>
        <source>Close all tabs to the right</source>
        <translation>Maak alle oortjies om regs toe</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1218"/>
        <location filename="../sources/mainwindow.cpp" line="1367"/>
        <source>Open</source>
        <translation>Oop</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1383"/>
        <source>Open in new tab</source>
        <translation>Maak oop in nuwe oortjie</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1487"/>
        <location filename="../sources/mainwindow.cpp" line="1504"/>
        <location filename="../sources/mainwindow.cpp" line="1719"/>
        <source>Could not refresh &apos;%1&apos;.</source>
        <translation>Kon nie verfris &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1530"/>
        <source>Save copy</source>
        <translation>Stoor kopie</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1540"/>
        <source>Could not save copy at &apos;%1&apos;.</source>
        <translation>Kon nie kopie stoor by nie &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1511"/>
        <source>Save as</source>
        <translation>Stoor as</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1452"/>
        <location filename="../sources/mainwindow.cpp" line="1463"/>
        <source>Move to instance</source>
        <translation>Skuif na instansie</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1452"/>
        <location filename="../sources/mainwindow.cpp" line="1463"/>
        <source>Failed to access instance &apos;%1&apos;.</source>
        <translation>Mislukte toegang tot instansie kry nie &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1498"/>
        <location filename="../sources/mainwindow.cpp" line="1520"/>
        <location filename="../sources/mainwindow.cpp" line="2823"/>
        <source>Could not save as &apos;%1&apos;.</source>
        <translation>Kon nie as stoor &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1578"/>
        <source>Could not print &apos;%1&apos;.</source>
        <translation>Kon nie afdruk &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1614"/>
        <source>Set first page</source>
        <translation>Stel eerste bladsy</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1614"/>
        <source>Select the first page of the body matter:</source>
        <translation>Selekteer die eerste bladsy van die liggaam saak maak:</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1627"/>
        <source>Jump to page</source>
        <translation>Spring na bladsy</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1627"/>
        <source>Page:</source>
        <translation>Bladsy:</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2950"/>
        <source>Jump to page %1</source>
        <translation>Spring na bladsy %1</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2160"/>
        <source>About qpdfview</source>
        <translation>Omtrent qpdfview</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2160"/>
        <source>&lt;p&gt;&lt;b&gt;qpdfview %1&lt;/b&gt;&lt;/p&gt;&lt;p&gt;qpdfview is a tabbed document viewer using Qt.&lt;/p&gt;&lt;p&gt;This version includes:&lt;ul&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;qpdfview %1&lt;/b&gt;&lt;/p&gt;&lt;p&gt;qpdfview is &apos;n oortjie-dokumentkyker wat gebruik maak van Qt.&lt;/p&gt;&lt;p&gt;Hierdie weergawe sluit in:&lt;ul&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2164"/>
        <source>&lt;li&gt;PDF support using Poppler %1&lt;/li&gt;</source>
        <translation>&lt;li&gt;PDF ondersteuning gebruik Poppler %1&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2167"/>
        <source>&lt;li&gt;PS support using libspectre %1&lt;/li&gt;</source>
        <translation>&lt;li&gt;PS ondersteuning gebruik libspectre %1&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2170"/>
        <source>&lt;li&gt;DjVu support using DjVuLibre %1&lt;/li&gt;</source>
        <translation>&lt;li&gt;DjVu ondersteuning gebruik DjVuLibre %1&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2173"/>
        <source>&lt;li&gt;PDF support using Fitz %1&lt;/li&gt;</source>
        <translation>&lt;li&gt;PDF ondersteuning gebruik Fitz %1&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2176"/>
        <source>&lt;li&gt;Printing support using CUPS %1&lt;/li&gt;</source>
        <translation>&lt;li&gt;Drukwerk ondersteuning gebruik CUPS %1&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2393"/>
        <source>&amp;Edit bookmark</source>
        <translation>&amp;Wysig boekmerk</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2813"/>
        <source>The document &apos;%1&apos; has been modified. Do you want to save your changes?</source>
        <translation>Die dokument &apos;%1&apos; het wees gewysig. Wil jy jou veranderinge stoor?</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3098"/>
        <source>Page width</source>
        <translation>Bladsy breedte</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3099"/>
        <source>Page size</source>
        <translation>Bladsy grootte</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3122"/>
        <source>Match &amp;case</source>
        <translation>Vuurhoutjie &amp;geval</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3123"/>
        <source>Whole &amp;words</source>
        <translation>Hele &amp;woorde</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3124"/>
        <source>Highlight &amp;all</source>
        <translation>Beklemtoon &amp;alle</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3184"/>
        <source>&amp;Open...</source>
        <translation>&amp;Maak oop...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3185"/>
        <source>Open in new &amp;tab...</source>
        <translation>Maak oop in nuwe &amp;oortjie...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3299"/>
        <source>Open &amp;copy in new tab</source>
        <translation>Maak &amp;kopieer in nuwe oortjie oop</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3302"/>
        <source>Move to &amp;instance...</source>
        <translation>Skuif na &amp;instansie...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3186"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Verfris</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1476"/>
        <source>Information</source>
        <translation>Inligting</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1476"/>
        <source>Instance-to-instance communication requires D-Bus support.</source>
        <translation>Instansie-tot-instansie-kommunikasie vereis D-Bus ondersteuning.</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2178"/>
        <source>&lt;/ul&gt;&lt;p&gt;See &lt;a href=&quot;https://launchpad.net/qpdfview&quot;&gt;launchpad.net/qpdfview&lt;/a&gt; for more information.&lt;/p&gt;&lt;p&gt;&amp;copy; %1 The qpdfview developers&lt;/p&gt;</source>
        <translation>&lt;/ul&gt;&lt;p&gt;Sien &lt;a href=&quot;https://launchpad.net/qpdfview&quot;&gt;launchpad.net/qpdfview&lt;/a&gt; vir meer inligting.&lt;/p&gt;&lt;p&gt;&amp;copy; %1 Die qpdfview ontwikkelaars&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3188"/>
        <source>Save &amp;as...</source>
        <translation>Stoor &amp;as...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3189"/>
        <source>Save &amp;copy...</source>
        <translation>Stoor &amp;kopieer...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3190"/>
        <source>&amp;Print...</source>
        <translation>&amp;Afdruk...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3191"/>
        <source>E&amp;xit</source>
        <translation>U&amp;itgang</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3196"/>
        <source>&amp;Previous page</source>
        <translation>&amp;Vorige bladsy</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3197"/>
        <source>&amp;Next page</source>
        <translation>&amp;Volgende bladsy</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3204"/>
        <source>&amp;First page</source>
        <translation>&amp;Eerste bladsy</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3211"/>
        <source>&amp;Last page</source>
        <translation>&amp;Laaste bladsy</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3213"/>
        <source>&amp;Set first page...</source>
        <translation>&amp;Stel eerste bladsy...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3215"/>
        <source>&amp;Jump to page...</source>
        <translation>&amp;Spring na bladsy...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3217"/>
        <source>Jump &amp;backward</source>
        <translation>Spring &amp;agteruit</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3218"/>
        <source>Jump for&amp;ward</source>
        <translation>Spring vor&amp;entoe</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3220"/>
        <source>&amp;Search...</source>
        <translation>&amp;Soek...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3221"/>
        <source>Find previous</source>
        <translation>Vind vorige</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3222"/>
        <source>Find next</source>
        <translation>Vind volgende</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3223"/>
        <source>Cancel search</source>
        <translation>Kanselleer soektog</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3225"/>
        <source>&amp;Copy to clipboard</source>
        <translation>&amp;Kopieer na knipbord</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3226"/>
        <source>&amp;Add annotation</source>
        <translation>&amp;Voeg annotasie</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3228"/>
        <source>Settings...</source>
        <translation>Instellings...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3233"/>
        <source>&amp;Continuous</source>
        <translation>&amp;Deurlopend</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3234"/>
        <source>&amp;Two pages</source>
        <translation>&amp;Twee bladsye</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3235"/>
        <source>Two pages &amp;with cover page</source>
        <translation>Twee bladsye &amp;met voorblad</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3236"/>
        <source>&amp;Multiple pages</source>
        <translation>&amp;Veelvuldige bladsye</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3238"/>
        <source>Right to left</source>
        <translation>Regs na links</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3239"/>
        <source>Zoom &amp;in</source>
        <translation>Zoem &amp;in</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3240"/>
        <source>Zoom &amp;out</source>
        <translation>Zoem &amp;uit</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3241"/>
        <source>Original &amp;size</source>
        <translation>Oorspronklike &amp;grootte</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3243"/>
        <source>Fit to page width</source>
        <translation>Fit by bladsy wydte</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3244"/>
        <source>Fit to page size</source>
        <translation>Pas by bladsy grootte</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3246"/>
        <source>Rotate &amp;left</source>
        <translation>Roteer &amp;links</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3247"/>
        <source>Rotate &amp;right</source>
        <translation>Roteer &amp;regs</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3249"/>
        <source>Invert colors</source>
        <translation>Keer kleure om</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3250"/>
        <source>Invert lightness</source>
        <translation>Keer ligheid om</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3251"/>
        <source>Convert to grayscale</source>
        <translation>Skakel om na grysskaal</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3252"/>
        <source>Trim margins</source>
        <translation>Sny marges</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3254"/>
        <source>Darken with paper color</source>
        <translation>Verdonker met papierkleur</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3255"/>
        <source>Lighten with paper color</source>
        <translation>Verlig met papierkleur</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3257"/>
        <source>Fonts...</source>
        <translation>Lettertipes...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3259"/>
        <source>&amp;Fullscreen</source>
        <translation>&amp;Volskerm</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3260"/>
        <source>&amp;Presentation...</source>
        <translation>&amp;Voorlegging...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3264"/>
        <source>&amp;Previous tab</source>
        <translation>&amp;Vorige oortjie</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3265"/>
        <source>&amp;Next tab</source>
        <translation>&amp;Volgende oortjie</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3267"/>
        <source>&amp;Close tab</source>
        <translation>&amp;Sluit oortjie</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3268"/>
        <source>Close &amp;all tabs</source>
        <translation>Sluit &amp;alle oortjies</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3269"/>
        <source>Close all tabs &amp;but current tab</source>
        <translation>Sluit alle oortjies &amp;maar huidige oortjie</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3271"/>
        <source>Restore &amp;most recently closed tab</source>
        <translation>Restoureer &amp;mees onlangs geslote oortjie</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3284"/>
        <source>&amp;Previous bookmark</source>
        <translation>&amp;Vorige boekmerk</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3285"/>
        <source>&amp;Next bookmark</source>
        <translation>&amp;Volgende boekmerk</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3287"/>
        <source>&amp;Add bookmark</source>
        <translation>&amp;Voeg Boekmerk</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3300"/>
        <source>Open copy in new &amp;window</source>
        <translation>Maak kopie oop in nuwe &amp;venster</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3303"/>
        <source>Split view horizontally...</source>
        <translation>Verdeel aansig horisontaal...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3304"/>
        <source>Split view vertically...</source>
        <translation>Split aansig vertikaal...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3305"/>
        <source>Close current view</source>
        <translation>Sluit huidige aansig</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3486"/>
        <source>Thumb&amp;nails</source>
        <translation>Duim&amp;naels</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3500"/>
        <source>Book&amp;marks</source>
        <translation>Boek&amp;merke</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3572"/>
        <source>Composition</source>
        <translation>Komposisie</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2392"/>
        <location filename="../sources/mainwindow.cpp" line="3288"/>
        <source>&amp;Remove bookmark</source>
        <translation>&amp;Verwyder boekmerk</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2995"/>
        <source>Edit &apos;%1&apos; at %2,%3...</source>
        <translation>Wysig &apos;%1&apos; by %2,%3...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3301"/>
        <source>Open containing &amp;folder</source>
        <translation>Maak bevat &amp;vouer oop</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3187"/>
        <source>&amp;Save</source>
        <translation>&amp;Stoor</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3289"/>
        <source>Remove all bookmarks</source>
        <translation>Verwyderalle boekmerke</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3293"/>
        <source>&amp;Contents</source>
        <translation>&amp;Inhoud</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3294"/>
        <source>&amp;About</source>
        <translation>&amp;Omtrent</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3333"/>
        <location filename="../sources/mainwindow.cpp" line="3527"/>
        <source>&amp;File</source>
        <translation>&amp;Lêer</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3336"/>
        <location filename="../sources/mainwindow.cpp" line="3548"/>
        <source>&amp;Edit</source>
        <translation>&amp;Wysig</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3339"/>
        <location filename="../sources/mainwindow.cpp" line="3561"/>
        <source>&amp;View</source>
        <translation>&amp;Bekyk</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3453"/>
        <source>&amp;Outline</source>
        <translation>&amp;Buitelyn</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3472"/>
        <source>&amp;Properties</source>
        <translation>&amp;Eiendomme</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3374"/>
        <source>&amp;Search</source>
        <translation>&amp;Soek</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3578"/>
        <source>&amp;Tool bars</source>
        <translation>&amp;Gereedskapstawe</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3581"/>
        <source>&amp;Docks</source>
        <translation>&amp;Docks</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3595"/>
        <source>&amp;Tabs</source>
        <translation>&amp;Oortjies</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3618"/>
        <source>&amp;Bookmarks</source>
        <translation>&amp;Boekmerke</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3629"/>
        <source>&amp;Help</source>
        <translation>&amp;Hulp</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="211"/>
        <location filename="../sources/mainwindow.cpp" line="1436"/>
        <source>Choose instance</source>
        <translation>Kies-instansie</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="211"/>
        <location filename="../sources/mainwindow.cpp" line="1436"/>
        <source>Instance:</source>
        <translation>Instansie:</translation>
    </message>
</context>
<context>
    <name>qpdfview::PageItem</name>
    <message>
        <location filename="../sources/pageitem.cpp" line="381"/>
        <source>Go to page %1.</source>
        <translation>Gaan na bladsy %1.</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="385"/>
        <source>Go to page %1 of file &apos;%2&apos;.</source>
        <translation>Gaan na bladsy %1 van lêer &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="393"/>
        <source>Open &apos;%1&apos;.</source>
        <translation>Maak oop &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="428"/>
        <source>Edit form field &apos;%1&apos;.</source>
        <translation>Wysig vormveld &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="796"/>
        <source>Copy &amp;text</source>
        <translation>Kopieer &amp;teks</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="797"/>
        <source>&amp;Select text</source>
        <translation>&amp;Selekteer teks</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="798"/>
        <source>&amp;Append text to bookmark comment...</source>
        <translation>&amp;Voeg teks by boekmerk opmerking...</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="799"/>
        <source>Copy &amp;image</source>
        <translation>Kopieer &amp;beeld</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="800"/>
        <source>Save image to &amp;file...</source>
        <translation>Stoor beeld na &amp;lêer...</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="840"/>
        <source>Save image to file</source>
        <translation>Stoor beeld na lêer</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="844"/>
        <source>Warning</source>
        <translation>Waarskuwing</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="844"/>
        <source>Could not save image to file &apos;%1&apos;.</source>
        <translation>Kon nie beeld in lêer stoor &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="857"/>
        <source>Add &amp;text</source>
        <translation>Byvoeg &amp;teks</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="858"/>
        <source>Add &amp;highlight</source>
        <translation>Byvoeg &amp;hoogtepunt</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="900"/>
        <source>&amp;Copy link address</source>
        <translation>&amp;Kopieer skakel adres</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="901"/>
        <source>&amp;Select link address</source>
        <translation>&amp;Selekteer skakel adres</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="924"/>
        <source>&amp;Remove annotation</source>
        <translation>&amp;Verwyder annotasie</translation>
    </message>
</context>
<context>
    <name>qpdfview::PdfSettingsWidget</name>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1172"/>
        <source>Antialiasing:</source>
        <translation>Antialiasing:</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1179"/>
        <source>Text antialiasing:</source>
        <translation>Teks antialiasing:</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1186"/>
        <location filename="../sources/pdfmodel.cpp" line="1225"/>
        <source>None</source>
        <translation>Geeneen</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1187"/>
        <source>Full</source>
        <translation>Volle</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1188"/>
        <source>Reduced</source>
        <translation>Verminderde</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1191"/>
        <location filename="../sources/pdfmodel.cpp" line="1198"/>
        <source>Text hinting:</source>
        <translation>Teks wenke:</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1207"/>
        <source>Ignore paper color:</source>
        <translation>Ignoreer papier kleur:</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1218"/>
        <source>Overprint preview:</source>
        <translation>Oordrukvoorskou:</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1226"/>
        <source>Solid</source>
        <translation>Solied</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1227"/>
        <source>Shaped</source>
        <translation>Gevorm</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1230"/>
        <source>Thin line mode:</source>
        <translation>Dun lyn modus:</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1235"/>
        <source>Splash</source>
        <translation>Spat</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1236"/>
        <source>Arthur</source>
        <translation>Outeur</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1239"/>
        <source>Backend:</source>
        <translation>Terugeinde:</translation>
    </message>
</context>
<context>
    <name>qpdfview::PluginHandler</name>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="453"/>
        <source>Image (%1)</source>
        <translation>Beeld (%1)</translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="468"/>
        <source>Compressed (%1)</source>
        <translation>Gecomprimeerd (%1)</translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="472"/>
        <source>Supported formats (%1)</source>
        <translation>Ondersteunde formate (%1)</translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="488"/>
        <source>Could not decompress &apos;%1&apos;!</source>
        <translation>Kon nie dekomprimeer &apos;%1&apos;!</translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="498"/>
        <source>Could not match file type of &apos;%1&apos;!</source>
        <translation>Kon nie ooreenstem met lêertipe van &apos;%1&apos;!</translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="505"/>
        <source>Critical</source>
        <translation>Kritiese</translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="505"/>
        <source>Could not load plug-in for file type &apos;%1&apos;!</source>
        <translation>Kon nie laai inprop vir lêer tipe &apos;%1&apos;!</translation>
    </message>
</context>
<context>
    <name>qpdfview::PrintDialog</name>
    <message>
        <location filename="../sources/printdialog.cpp" line="76"/>
        <source>Fit to page:</source>
        <translation>Pas aan bladsy:</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="81"/>
        <source>e.g. 3-4,7,8,9-11</source>
        <translation>bv. 3-4,7,8,9-11</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="83"/>
        <source>Page ranges:</source>
        <translation>Bladsy ranges:</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="90"/>
        <source>All pages</source>
        <translation>Alle bladsye</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="91"/>
        <source>Even pages</source>
        <translation>Ewe bladsye</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="92"/>
        <source>Odd pages</source>
        <translation>Onewe bladsye</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="95"/>
        <source>Page set:</source>
        <translation>Bladsy stel:</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="98"/>
        <source>Single page</source>
        <translation>Enkel bladsy</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="99"/>
        <source>Two pages</source>
        <translation>Twee bladsye</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="100"/>
        <source>Four pages</source>
        <translation>Vier bladsye</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="101"/>
        <source>Six pages</source>
        <translation>Ses bladsye</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="102"/>
        <source>Nine pages</source>
        <translation>Nege bladsye</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="103"/>
        <source>Sixteen pages</source>
        <translation>Sestien bladsye</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="106"/>
        <source>Number-up:</source>
        <translation>Nommer-up:</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="109"/>
        <source>Bottom to top and left to right</source>
        <translation>Bodem na bopunt en links na regs</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="110"/>
        <source>Bottom to top and right to left</source>
        <translation>Bodem na bopunt en regs na links</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="111"/>
        <source>Left to right and bottom to top</source>
        <translation>Links na regs en bodem na bopunt</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="112"/>
        <source>Left to right and top to bottom</source>
        <translation>Links na regs en bopunt na bodem</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="113"/>
        <source>Right to left and bottom to top</source>
        <translation>Regs na links en bodem na bopunt</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="114"/>
        <source>Right to left and top to bottom</source>
        <translation>Regs na links en bopunt na bodem</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="115"/>
        <source>Top to bottom and left to right</source>
        <translation>Bopunt na bodem en links na regs</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="116"/>
        <source>Top to bottom and right to left</source>
        <translation>Bopunt na bodem en regs na links</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="119"/>
        <source>Number-up layout:</source>
        <translation>Nommer-up uitleg:</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="123"/>
        <source>Extended options</source>
        <translation>Verlengde opsies</translation>
    </message>
</context>
<context>
    <name>qpdfview::PsSettingsWidget</name>
    <message>
        <location filename="../sources/psmodel.cpp" line="262"/>
        <source>Graphics antialias bits:</source>
        <translation>Grafiese antialias bisse:</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="270"/>
        <source>Text antialias bits:</source>
        <translation>Teks antialias bisse:</translation>
    </message>
</context>
<context>
    <name>qpdfview::RecentlyClosedMenu</name>
    <message>
        <location filename="../sources/recentlyclosedmenu.cpp" line="34"/>
        <source>&amp;Recently closed</source>
        <translation>&amp;Onlangs gesluit</translation>
    </message>
    <message>
        <location filename="../sources/recentlyclosedmenu.cpp" line="41"/>
        <source>&amp;Clear list</source>
        <translation>&amp;Maak lys skoon</translation>
    </message>
</context>
<context>
    <name>qpdfview::RecentlyUsedMenu</name>
    <message>
        <location filename="../sources/recentlyusedmenu.cpp" line="33"/>
        <source>Recently &amp;used</source>
        <translation>Onlangs &amp;gebruikde</translation>
    </message>
    <message>
        <location filename="../sources/recentlyusedmenu.cpp" line="41"/>
        <source>&amp;Clear list</source>
        <translation>&amp;Maak lys skoon</translation>
    </message>
</context>
<context>
    <name>qpdfview::SearchModel</name>
    <message>
        <location filename="../sources/searchmodel.cpp" line="154"/>
        <source>&lt;b&gt;%1&lt;/b&gt; occurrences</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; voorkomste</translation>
    </message>
    <message>
        <location filename="../sources/searchmodel.cpp" line="198"/>
        <source>&lt;b&gt;%1&lt;/b&gt; occurrences on page &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; voorkomste op bladsy &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>qpdfview::SearchableMenu</name>
    <message>
        <location filename="../sources/miscellaneous.cpp" line="210"/>
        <source>Search for &apos;%1&apos;...</source>
        <translation>Soek vir &apos;%1&apos;...</translation>
    </message>
</context>
<context>
    <name>qpdfview::SettingsDialog</name>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="114"/>
        <source>General</source>
        <translation>Generaal</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="147"/>
        <source>&amp;Behavior</source>
        <translation>&amp;Gedrag</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="148"/>
        <source>&amp;Graphics</source>
        <translation>&amp;Grafika</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="149"/>
        <source>&amp;Interface</source>
        <translation>&amp;Koppelvlak</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="150"/>
        <source>&amp;Shortcuts</source>
        <translation>&amp;Kortpaaie</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="151"/>
        <source>&amp;Modifiers</source>
        <translation>&amp;Modifiseers</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="157"/>
        <source>Defaults</source>
        <translation>Verstek</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="160"/>
        <source>Defaults on current tab</source>
        <translation>Verstek op huidige oortjie</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="166"/>
        <source>Mouse wheel modifiers</source>
        <translation>Muis wiel modifiseers</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="169"/>
        <source>Mouse button modifiers</source>
        <translation>Muis knoppie modifiseers</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="250"/>
        <source>Open URL:</source>
        <translation>Oop URL</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="254"/>
        <source>Auto-refresh:</source>
        <translation>Outo-verversing:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="258"/>
        <location filename="../sources/settingsdialog.cpp" line="261"/>
        <location filename="../sources/settingsdialog.cpp" line="554"/>
        <location filename="../sources/settingsdialog.cpp" line="585"/>
        <location filename="../sources/settingsdialog.cpp" line="588"/>
        <location filename="../sources/settingsdialog.cpp" line="592"/>
        <location filename="../sources/settingsdialog.cpp" line="595"/>
        <location filename="../sources/settingsdialog.cpp" line="598"/>
        <location filename="../sources/settingsdialog.cpp" line="607"/>
        <source>Effective after restart.</source>
        <translation>Effektief na herbegin.</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="258"/>
        <source>Track recently used:</source>
        <translation>Spoor onlangs gebruik:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="261"/>
        <source>Keep recently closed:</source>
        <translation>Hou- onlangs gesluit:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="265"/>
        <source>Restore tabs:</source>
        <translation>Restoureer oortjies:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="268"/>
        <source>Restore bookmarks:</source>
        <translation>Restoureer boekmerke:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="271"/>
        <source>Restore per-file settings:</source>
        <translation>Restoureer per-lêer instellings:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="274"/>
        <source> min</source>
        <translation> min</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="274"/>
        <source>Save database interval:</source>
        <translation>Stoor databasis interval:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="297"/>
        <source>Synchronize presentation:</source>
        <translation>Sinkroniseer aanbieding:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="300"/>
        <source>Default</source>
        <translation>Verstek</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="300"/>
        <source>Presentation screen:</source>
        <translation>Aanbieding skerm:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="307"/>
        <source>Synchronize split views:</source>
        <translation>Sinkroniseer gesplete aansigte:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="311"/>
        <source>Relative jumps:</source>
        <translation>Relatiewe spronge:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="317"/>
        <source>Zoom factor:</source>
        <translation>Zoem faktor:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="320"/>
        <source>Parallel search execution:</source>
        <translation>Parallelle soektog uitvoering:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="324"/>
        <source> ms</source>
        <translation> ms</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="324"/>
        <source>None</source>
        <translation>Geeneen</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="324"/>
        <source>Highlight duration:</source>
        <translation>Hoogtepunt duur:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="327"/>
        <source>Highlight color:</source>
        <translation>Hoogtepunt kleur:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="330"/>
        <source>Annotation color:</source>
        <translation>Aantekening kleur:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="334"/>
        <source>&apos;%1&apos; is replaced by the absolute file path. &apos;%2&apos; resp. &apos;%3&apos; is replaced by line resp. column number.</source>
        <translation>&apos;%1&apos; word vervang deur die absolute lêerpad. &apos;%2&apos; resp. &apos;%3&apos; word vervang deur lyn resp. kolom nommer.</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="334"/>
        <source>Source editor:</source>
        <translation>Bron redakteur:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="404"/>
        <source>Use tiling:</source>
        <translation>Gebruik teëlwerk:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="407"/>
        <source>Keep obsolete pixmaps:</source>
        <translation>Hou uitgediende pixmaps:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="410"/>
        <source>Use device pixel ratio:</source>
        <translation>Gebruik toestel pixel verhouding:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="419"/>
        <source>Use logical DPI:</source>
        <translation>Gebruik logiese DPI:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="423"/>
        <source>Decorate pages:</source>
        <translation>Versier bladsye:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="426"/>
        <source>Decorate links:</source>
        <translation>Versier skakels:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="429"/>
        <source>Decorate form fields:</source>
        <translation>Versier vorm velde</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="433"/>
        <source>Background color:</source>
        <translation>Agtergrond kleur:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="436"/>
        <source>Paper color:</source>
        <translation>Papier kleur:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="439"/>
        <source>Presentation background color:</source>
        <translation>Aanbieding agtergrond kleur:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="443"/>
        <source>Pages per row:</source>
        <translation>Bladsye per ry:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="447"/>
        <location filename="../sources/settingsdialog.cpp" line="450"/>
        <location filename="../sources/settingsdialog.cpp" line="454"/>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="447"/>
        <source>Page spacing:</source>
        <translation>Bladsy spasiëring:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="450"/>
        <source>Thumbnail spacing:</source>
        <translation>Duimnael spasiëring:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="454"/>
        <source>Thumbnail size:</source>
        <translation>Duimnael grootte:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="454"/>
        <source>Fit to viewport</source>
        <translation>Fit om te uitsig-poort</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="601"/>
        <source>Document context menu:</source>
        <translation>Dokument kontek spyskaart:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="604"/>
        <source>Tab context menu:</source>
        <translation>Oortjie konteks spyskaart:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="737"/>
        <source>Open in source editor:</source>
        <translation>Maak oop in bron redigeerder:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="848"/>
        <location filename="../sources/settingsdialog.cpp" line="857"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="458"/>
        <source>Cache size:</source>
        <translation>Cache grootte:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="314"/>
        <source>Minimal scrolling:</source>
        <translation>Minimale scrolling:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="461"/>
        <source>Prefetch:</source>
        <translation>Prefetch:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="464"/>
        <source>Prefetch distance:</source>
        <translation>Prefetch afstand:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="565"/>
        <source>Top</source>
        <translation>Bopunt</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="565"/>
        <source>Bottom</source>
        <translation>Bodem</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="565"/>
        <source>Left</source>
        <translation>Links</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="565"/>
        <source>Right</source>
        <translation>Regs</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="564"/>
        <source>Tab position:</source>
        <translation>Oortjie-posisie:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="570"/>
        <source>As needed</source>
        <translation>Soos benodig</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="570"/>
        <source>Always</source>
        <translation>Altyd</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="581"/>
        <source>Exit after last tab:</source>
        <translation>Uitgang nadat laaste oortjie:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="607"/>
        <source>Scrollable menus:</source>
        <translation>Scrollable spyskaarte:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="610"/>
        <source>Searchable menus:</source>
        <translation>Soekbare spyskaarte:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="734"/>
        <source>Zoom to selection:</source>
        <translation>Zoem om seleksie:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="274"/>
        <location filename="../sources/settingsdialog.cpp" line="570"/>
        <source>Never</source>
        <translation>Nooit</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="111"/>
        <source>Settings</source>
        <translation>Instellings</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="569"/>
        <source>Tab visibility:</source>
        <translation>Oortjie-sigbaarheid:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="574"/>
        <source>Spread tabs:</source>
        <translation>Sprei oortjies uit:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="578"/>
        <source>New tab next to current tab:</source>
        <translation>Nuwe oortjie langs huidige oortjie:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="585"/>
        <source>Recently used count:</source>
        <translation>Onlangs gebruik getelling:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="588"/>
        <source>Recently closed count:</source>
        <translation>Onlangs geslote getelling:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="554"/>
        <source>Extended search dock:</source>
        <translation>Verlengde soekdok:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="614"/>
        <source>Toggle tool and menu bars with fullscreen:</source>
        <translation>Toggle nuts en spyskaartbalke met volskerm:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="592"/>
        <source>File tool bar:</source>
        <translation>Lêer werkbalk:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="595"/>
        <source>Edit tool bar:</source>
        <translation>Wysig nutsbalk:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="598"/>
        <source>View tool bar:</source>
        <translation>Bekyk nutsbalk:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="618"/>
        <source>Use page label:</source>
        <translation>Gebruik bladsy etiket:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="621"/>
        <source>Document title as tab title:</source>
        <translation>Dokument titel as oortjie titel:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="625"/>
        <source>Current page in window title:</source>
        <translation>Huidige bladsy in venster titel:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="628"/>
        <source>Instance name in window title:</source>
        <translation>Geval naam in venster titel:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="304"/>
        <source>Synchronize outline view:</source>
        <translation>Sinkroniseer buitelyn aansig:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="632"/>
        <source>Highlight current thumbnail:</source>
        <translation>Hoogtepunt huidige duimnael:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="635"/>
        <source>Limit thumbnails to results:</source>
        <translation>Limiet duimnaels na resultate:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="557"/>
        <source>Annotation overlay:</source>
        <translation>Annotasie overlay:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="560"/>
        <source>Form field overlay:</source>
        <translation>Vorm veld overlay:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="719"/>
        <source>Zoom:</source>
        <translation>Zoem:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="722"/>
        <source>Rotate:</source>
        <translation>Roteer</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="725"/>
        <source>Scroll:</source>
        <translation>Scroll:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="728"/>
        <source>Copy to clipboard:</source>
        <translation>Kopieer na knipbord:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="731"/>
        <source>Add annotation:</source>
        <translation>Byvoeg annotasie:</translation>
    </message>
</context>
<context>
    <name>qpdfview::ShortcutHandler</name>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="147"/>
        <source>Action</source>
        <translation>Aksie</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="149"/>
        <source>Key sequence</source>
        <translation>Sleutel-volgorde</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="362"/>
        <source>Skip backward</source>
        <translation>Spring agtertoe</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="366"/>
        <source>Skip forward</source>
        <translation>Spring vorentoe</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="370"/>
        <source>Move up</source>
        <translation>Skuif op</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="374"/>
        <source>Move down</source>
        <translation>Beweeg ondertoe</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="378"/>
        <source>Move left</source>
        <translation>Beweeg links</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="382"/>
        <source>Move right</source>
        <translation>Beweeg regs</translation>
    </message>
</context>
<context>
    <name>qpdfview::TreeView</name>
    <message>
        <location filename="../sources/miscellaneous.cpp" line="710"/>
        <source>&amp;Expand all</source>
        <translation>&amp;Uitbrei alle</translation>
    </message>
    <message>
        <location filename="../sources/miscellaneous.cpp" line="711"/>
        <source>&amp;Collapse all</source>
        <translation>&amp;Ineenstort alle</translation>
    </message>
</context>
</TS>
