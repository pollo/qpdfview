<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="rue">
    <extra-po-header-po_revision_date>2022-07-01 17:57+0000</extra-po-header-po_revision_date>
    <extra-po-header-x_launchpad_export_date>2022-07-02 04:46+0000</extra-po-header-x_launchpad_export_date>
    <extra-po-headers>Project-Id-Version,Report-Msgid-Bugs-To,POT-Creation-Date,PO-Revision-Date,Last-Translator,Language-Team,MIME-Version,Content-Type,Content-Transfer-Encoding,X-Launchpad-Export-Date,X-Qt-Contexts</extra-po-headers>
    <extra-po-header-report_msgid_bugs_to>FULL NAME &lt;EMAIL@ADDRESS&gt;</extra-po-header-report_msgid_bugs_to>
    <extra-po-header-language_team>Rusyn &lt;rue@li.org&gt;</extra-po-header-language_team>
    <extra-po-header-project_id_version>qpdfview</extra-po-header-project_id_version>
    <extra-po-header-pot_creation_date>2021-04-23 17:12+0000</extra-po-header-pot_creation_date>
    <extra-po-header_comment># Rusyn translation for qpdfview
# Copyright (c) 2022 Rosetta Contributors and Canonical Ltd 2022
# This file is distributed under the same license as the qpdfview package.
# FIRST AUTHOR &lt;EMAIL@ADDRESS&gt;, 2022.
#</extra-po-header_comment>
    <extra-po-header-last_translator>Vondrosh Tenkač &lt;Unknown&gt;</extra-po-header-last_translator>
<context>
    <name>Model::ImageDocument</name>
    <message>
        <location filename="../sources/imagemodel.cpp" line="121"/>
        <source>Image (%1)</source>
        <translation>Картинка (%1)</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="149"/>
        <source>Size</source>
        <translation>Розмір</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="150"/>
        <source>Resolution</source>
        <translation>Розрішеня</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="151"/>
        <source>Depth</source>
        <translation>Глубина</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="159"/>
        <location filename="../sources/imagemodel.cpp" line="162"/>
        <location filename="../sources/imagemodel.cpp" line="165"/>
        <location filename="../sources/imagemodel.cpp" line="168"/>
        <location filename="../sources/imagemodel.cpp" line="173"/>
        <location filename="../sources/imagemodel.cpp" line="177"/>
        <source>Format</source>
        <translation>Формат</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="159"/>
        <source>Monochrome</source>
        <translation>Монохромный</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="162"/>
        <source>Indexed</source>
        <translation>Індексірованноє</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="165"/>
        <source>32 bits RGB</source>
        <translation>32 біт РҐБ</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="168"/>
        <source>32 bits ARGB</source>
        <translation>32 біт АРҐБ</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="173"/>
        <source>16 bits RGB</source>
        <translation>16 біт РҐБ</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="177"/>
        <source>24 bits RGB</source>
        <translation>24 біт РҐБ</translation>
    </message>
</context>
<context>
    <name>Model::PdfDocument</name>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1094"/>
        <source>Linearized</source>
        <translation>Лінеарізованый</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="248"/>
        <source>Name</source>
        <translation>Имня</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="250"/>
        <source>Type</source>
        <translation>Тіп</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="252"/>
        <source>Embedded</source>
        <translation>Увнаџеноє</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="254"/>
        <source>Subset</source>
        <translation>Сабсет</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="256"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="278"/>
        <location filename="../sources/pdfmodel.cpp" line="280"/>
        <location filename="../sources/pdfmodel.cpp" line="1093"/>
        <location filename="../sources/pdfmodel.cpp" line="1094"/>
        <source>Yes</source>
        <translation>Айно</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1091"/>
        <source>PDF version</source>
        <translation>PDF верзія</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1093"/>
        <source>Encrypted</source>
        <translation>Зашіфровано</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="278"/>
        <location filename="../sources/pdfmodel.cpp" line="280"/>
        <location filename="../sources/pdfmodel.cpp" line="1093"/>
        <location filename="../sources/pdfmodel.cpp" line="1094"/>
        <source>No</source>
        <translation>Ньит</translation>
    </message>
</context>
<context>
    <name>Model::PdfPage</name>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="767"/>
        <source>Information</source>
        <translation>Інформацїя</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="767"/>
        <source>Version 0.20.1 or higher of the Poppler library is required to add or remove annotations.</source>
        <translation>Діля того обо придати вадь одняти аннотації трібно ємнити бібліотеку Poppler верзії 0.20.1 вадь вышше.</translation>
    </message>
</context>
<context>
    <name>Model::PsDocument</name>
    <message>
        <location filename="../sources/psmodel.cpp" line="239"/>
        <source>Title</source>
        <translation>Назывка</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="240"/>
        <source>Created for</source>
        <translation>Учинено діля</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="241"/>
        <source>Creator</source>
        <translation>Созидатель</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="242"/>
        <source>Creation date</source>
        <translation>Дата созиданя</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="243"/>
        <source>Format</source>
        <translation>Формат</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="244"/>
        <source>Language level</source>
        <translation>Рувень бисїды</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../sources/main.cpp" line="172"/>
        <source>An empty instance name is not allowed.</source>
        <translation>Пуста назывка ся не припускат.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="183"/>
        <source>An empty search text is not allowed.</source>
        <translation>Пустый текст гляданя ся не припускат.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="240"/>
        <source>Unknown command-line option &apos;%1&apos;.</source>
        <translation>Незнамый параметр командной строкы &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="271"/>
        <source>Using &apos;--instance&apos; requires an instance name.</source>
        <translation>Для хоснованя &apos;--instance&apos; трібно имня екземплара</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="277"/>
        <source>Using &apos;--instance&apos; is not allowed without using &apos;--unique&apos;.</source>
        <translation>Хоснованя &apos;--instance&apos; не припускат ся без хоснованя &apos;--unique&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="283"/>
        <source>An instance name must only contain the characters &quot;[A-Z][a-z][0-9]_&quot; and must not begin with a digit.</source>
        <translation>Имня екземплара має ємнити лем сімболы &quot;[AZ][az][0-9]_&quot; тай не має зачинати ся из ціфры.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="289"/>
        <source>Using &apos;--search&apos; requires a search text.</source>
        <translation>Хоснуйте &apos;--search&apos; обо глядати текст.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="449"/>
        <source>Could not prepare signal handler.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QShortcut</name>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="886"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="887"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="888"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="889"/>
        <source>Shift and Ctrl</source>
        <translation>Shift тай Ctrl</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="890"/>
        <source>Shift and Alt</source>
        <translation>Shift тай  Alt</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="891"/>
        <source>Ctrl and Alt</source>
        <translation>Ctrl тай Alt</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="892"/>
        <source>Right mouse button</source>
        <translation>Права клопка</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="893"/>
        <source>Middle mouse button</source>
        <translation>Серидня клопка мышы</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="894"/>
        <source>None</source>
        <translation>Неє</translation>
    </message>
</context>
<context>
    <name>qpdfview::BookmarkDialog</name>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="40"/>
        <source>Bookmark</source>
        <translation>Марча</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="49"/>
        <source>Page:</source>
        <translation>Сторунка:</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="54"/>
        <source>Label:</source>
        <translation>Закладка:</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="59"/>
        <source>Comment:</source>
        <translation>Коментарій:</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="65"/>
        <source>Modified:</source>
        <translation>Модіфіціровано:</translation>
    </message>
</context>
<context>
    <name>qpdfview::BookmarkMenu</name>
    <message>
        <location filename="../sources/bookmarkmenu.cpp" line="42"/>
        <source>&amp;Open</source>
        <translation>&amp;Удкрыти</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkmenu.cpp" line="46"/>
        <source>Open in new &amp;tab</source>
        <translation>Удкрыто у новому &amp;tab</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkmenu.cpp" line="55"/>
        <source>&amp;Remove bookmark</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::Database</name>
    <message>
        <location filename="../sources/database.cpp" line="993"/>
        <source>Jump to page %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::DocumentView</name>
    <message>
        <location filename="../sources/documentview.cpp" line="1446"/>
        <location filename="../sources/documentview.cpp" line="2084"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="1446"/>
        <source>The source editor has not been set.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2084"/>
        <source>Opening URL is disabled in the settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2124"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2124"/>
        <location filename="../sources/main.cpp" line="365"/>
        <source>SyncTeX data for &apos;%1&apos; could not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2643"/>
        <source>Printing &apos;%1&apos;...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2717"/>
        <source>Unlock %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2717"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="538"/>
        <source>Page %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::FileAttachmentAnnotationWidget</name>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="116"/>
        <source>Save...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="117"/>
        <source>Save and open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="164"/>
        <source>Save file attachment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="180"/>
        <location filename="../sources/annotationwidgets.cpp" line="186"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="180"/>
        <source>Could not open file attachment saved to &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="186"/>
        <source>Could not save file attachment to &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::FontsDialog</name>
    <message>
        <location filename="../sources/fontsdialog.cpp" line="37"/>
        <source>Fonts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::HelpDialog</name>
    <message>
        <location filename="../sources/helpdialog.cpp" line="41"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/helpdialog.cpp" line="48"/>
        <source>help.html</source>
        <extracomment>Please replace by file name of localized help if available, e.g. &quot;help_fr.html&quot;.
</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/helpdialog.cpp" line="63"/>
        <source>Find previous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/helpdialog.cpp" line="67"/>
        <source>Find next</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::MainWindow</name>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3309"/>
        <source>Toggle tool bars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3310"/>
        <source>Toggle menu bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="484"/>
        <location filename="../sources/mainwindow.cpp" line="526"/>
        <location filename="../sources/mainwindow.cpp" line="1487"/>
        <location filename="../sources/mainwindow.cpp" line="1498"/>
        <location filename="../sources/mainwindow.cpp" line="1504"/>
        <location filename="../sources/mainwindow.cpp" line="1520"/>
        <location filename="../sources/mainwindow.cpp" line="1540"/>
        <location filename="../sources/mainwindow.cpp" line="1578"/>
        <location filename="../sources/mainwindow.cpp" line="1719"/>
        <location filename="../sources/mainwindow.cpp" line="2813"/>
        <location filename="../sources/mainwindow.cpp" line="2823"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="484"/>
        <location filename="../sources/mainwindow.cpp" line="526"/>
        <source>Could not open &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="798"/>
        <source>Copy file path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="799"/>
        <source>Select file path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="801"/>
        <source>Close all tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="802"/>
        <source>Close all tabs but this one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="803"/>
        <source>Close all tabs to the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="804"/>
        <source>Close all tabs to the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1218"/>
        <location filename="../sources/mainwindow.cpp" line="1367"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1383"/>
        <source>Open in new tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1487"/>
        <location filename="../sources/mainwindow.cpp" line="1504"/>
        <location filename="../sources/mainwindow.cpp" line="1719"/>
        <source>Could not refresh &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1530"/>
        <source>Save copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1540"/>
        <source>Could not save copy at &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1511"/>
        <source>Save as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1452"/>
        <location filename="../sources/mainwindow.cpp" line="1463"/>
        <source>Move to instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1452"/>
        <location filename="../sources/mainwindow.cpp" line="1463"/>
        <source>Failed to access instance &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1498"/>
        <location filename="../sources/mainwindow.cpp" line="1520"/>
        <location filename="../sources/mainwindow.cpp" line="2823"/>
        <source>Could not save as &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1578"/>
        <source>Could not print &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1614"/>
        <source>Set first page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1614"/>
        <source>Select the first page of the body matter:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1627"/>
        <source>Jump to page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1627"/>
        <source>Page:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2950"/>
        <source>Jump to page %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2160"/>
        <source>About qpdfview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2160"/>
        <source>&lt;p&gt;&lt;b&gt;qpdfview %1&lt;/b&gt;&lt;/p&gt;&lt;p&gt;qpdfview is a tabbed document viewer using Qt.&lt;/p&gt;&lt;p&gt;This version includes:&lt;ul&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2164"/>
        <source>&lt;li&gt;PDF support using Poppler %1&lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2167"/>
        <source>&lt;li&gt;PS support using libspectre %1&lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2170"/>
        <source>&lt;li&gt;DjVu support using DjVuLibre %1&lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2173"/>
        <source>&lt;li&gt;PDF support using Fitz %1&lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2176"/>
        <source>&lt;li&gt;Printing support using CUPS %1&lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2393"/>
        <source>&amp;Edit bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2813"/>
        <source>The document &apos;%1&apos; has been modified. Do you want to save your changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3098"/>
        <source>Page width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3099"/>
        <source>Page size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3122"/>
        <source>Match &amp;case</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3123"/>
        <source>Whole &amp;words</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3124"/>
        <source>Highlight &amp;all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3184"/>
        <source>&amp;Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3185"/>
        <source>Open in new &amp;tab...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3299"/>
        <source>Open &amp;copy in new tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3302"/>
        <source>Move to &amp;instance...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3186"/>
        <source>&amp;Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1476"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1476"/>
        <source>Instance-to-instance communication requires D-Bus support.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2178"/>
        <source>&lt;/ul&gt;&lt;p&gt;See &lt;a href=&quot;https://launchpad.net/qpdfview&quot;&gt;launchpad.net/qpdfview&lt;/a&gt; for more information.&lt;/p&gt;&lt;p&gt;&amp;copy; %1 The qpdfview developers&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3188"/>
        <source>Save &amp;as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3189"/>
        <source>Save &amp;copy...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3190"/>
        <source>&amp;Print...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3191"/>
        <source>E&amp;xit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3196"/>
        <source>&amp;Previous page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3197"/>
        <source>&amp;Next page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3204"/>
        <source>&amp;First page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3211"/>
        <source>&amp;Last page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3213"/>
        <source>&amp;Set first page...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3215"/>
        <source>&amp;Jump to page...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3217"/>
        <source>Jump &amp;backward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3218"/>
        <source>Jump for&amp;ward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3220"/>
        <source>&amp;Search...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3221"/>
        <source>Find previous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3222"/>
        <source>Find next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3223"/>
        <source>Cancel search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3225"/>
        <source>&amp;Copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3226"/>
        <source>&amp;Add annotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3228"/>
        <source>Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3233"/>
        <source>&amp;Continuous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3234"/>
        <source>&amp;Two pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3235"/>
        <source>Two pages &amp;with cover page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3236"/>
        <source>&amp;Multiple pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3238"/>
        <source>Right to left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3239"/>
        <source>Zoom &amp;in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3240"/>
        <source>Zoom &amp;out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3241"/>
        <source>Original &amp;size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3243"/>
        <source>Fit to page width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3244"/>
        <source>Fit to page size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3246"/>
        <source>Rotate &amp;left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3247"/>
        <source>Rotate &amp;right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3249"/>
        <source>Invert colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3250"/>
        <source>Invert lightness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3251"/>
        <source>Convert to grayscale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3252"/>
        <source>Trim margins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3254"/>
        <source>Darken with paper color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3255"/>
        <source>Lighten with paper color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3257"/>
        <source>Fonts...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3259"/>
        <source>&amp;Fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3260"/>
        <source>&amp;Presentation...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3264"/>
        <source>&amp;Previous tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3265"/>
        <source>&amp;Next tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3267"/>
        <source>&amp;Close tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3268"/>
        <source>Close &amp;all tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3269"/>
        <source>Close all tabs &amp;but current tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3271"/>
        <source>Restore &amp;most recently closed tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3284"/>
        <source>&amp;Previous bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3285"/>
        <source>&amp;Next bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3287"/>
        <source>&amp;Add bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3300"/>
        <source>Open copy in new &amp;window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3303"/>
        <source>Split view horizontally...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3304"/>
        <source>Split view vertically...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3305"/>
        <source>Close current view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3486"/>
        <source>Thumb&amp;nails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3500"/>
        <source>Book&amp;marks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3572"/>
        <source>Composition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2392"/>
        <location filename="../sources/mainwindow.cpp" line="3288"/>
        <source>&amp;Remove bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2995"/>
        <source>Edit &apos;%1&apos; at %2,%3...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3301"/>
        <source>Open containing &amp;folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3187"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3289"/>
        <source>Remove all bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3293"/>
        <source>&amp;Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3294"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3333"/>
        <location filename="../sources/mainwindow.cpp" line="3527"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3336"/>
        <location filename="../sources/mainwindow.cpp" line="3548"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3339"/>
        <location filename="../sources/mainwindow.cpp" line="3561"/>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3453"/>
        <source>&amp;Outline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3472"/>
        <source>&amp;Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3374"/>
        <source>&amp;Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3578"/>
        <source>&amp;Tool bars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3581"/>
        <source>&amp;Docks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3595"/>
        <source>&amp;Tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3618"/>
        <source>&amp;Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3629"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="211"/>
        <location filename="../sources/mainwindow.cpp" line="1436"/>
        <source>Choose instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="211"/>
        <location filename="../sources/mainwindow.cpp" line="1436"/>
        <source>Instance:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::PageItem</name>
    <message>
        <location filename="../sources/pageitem.cpp" line="381"/>
        <source>Go to page %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="385"/>
        <source>Go to page %1 of file &apos;%2&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="393"/>
        <source>Open &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="428"/>
        <source>Edit form field &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="796"/>
        <source>Copy &amp;text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="797"/>
        <source>&amp;Select text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="798"/>
        <source>&amp;Append text to bookmark comment...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="799"/>
        <source>Copy &amp;image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="800"/>
        <source>Save image to &amp;file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="840"/>
        <source>Save image to file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="844"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="844"/>
        <source>Could not save image to file &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="857"/>
        <source>Add &amp;text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="858"/>
        <source>Add &amp;highlight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="900"/>
        <source>&amp;Copy link address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="901"/>
        <source>&amp;Select link address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="924"/>
        <source>&amp;Remove annotation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::PdfSettingsWidget</name>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1172"/>
        <source>Antialiasing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1179"/>
        <source>Text antialiasing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1186"/>
        <location filename="../sources/pdfmodel.cpp" line="1225"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1187"/>
        <source>Full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1188"/>
        <source>Reduced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1191"/>
        <location filename="../sources/pdfmodel.cpp" line="1198"/>
        <source>Text hinting:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1207"/>
        <source>Ignore paper color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1218"/>
        <source>Overprint preview:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1226"/>
        <source>Solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1227"/>
        <source>Shaped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1230"/>
        <source>Thin line mode:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1235"/>
        <source>Splash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1236"/>
        <source>Arthur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1239"/>
        <source>Backend:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::PluginHandler</name>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="453"/>
        <source>Image (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="468"/>
        <source>Compressed (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="472"/>
        <source>Supported formats (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="488"/>
        <source>Could not decompress &apos;%1&apos;!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="498"/>
        <source>Could not match file type of &apos;%1&apos;!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="505"/>
        <source>Critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="505"/>
        <source>Could not load plug-in for file type &apos;%1&apos;!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::PrintDialog</name>
    <message>
        <location filename="../sources/printdialog.cpp" line="76"/>
        <source>Fit to page:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="81"/>
        <source>e.g. 3-4,7,8,9-11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="83"/>
        <source>Page ranges:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="90"/>
        <source>All pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="91"/>
        <source>Even pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="92"/>
        <source>Odd pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="95"/>
        <source>Page set:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="98"/>
        <source>Single page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="99"/>
        <source>Two pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="100"/>
        <source>Four pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="101"/>
        <source>Six pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="102"/>
        <source>Nine pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="103"/>
        <source>Sixteen pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="106"/>
        <source>Number-up:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="109"/>
        <source>Bottom to top and left to right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="110"/>
        <source>Bottom to top and right to left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="111"/>
        <source>Left to right and bottom to top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="112"/>
        <source>Left to right and top to bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="113"/>
        <source>Right to left and bottom to top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="114"/>
        <source>Right to left and top to bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="115"/>
        <source>Top to bottom and left to right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="116"/>
        <source>Top to bottom and right to left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="119"/>
        <source>Number-up layout:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="123"/>
        <source>Extended options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::PsSettingsWidget</name>
    <message>
        <location filename="../sources/psmodel.cpp" line="262"/>
        <source>Graphics antialias bits:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="270"/>
        <source>Text antialias bits:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::RecentlyClosedMenu</name>
    <message>
        <location filename="../sources/recentlyclosedmenu.cpp" line="34"/>
        <source>&amp;Recently closed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/recentlyclosedmenu.cpp" line="41"/>
        <source>&amp;Clear list</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::RecentlyUsedMenu</name>
    <message>
        <location filename="../sources/recentlyusedmenu.cpp" line="33"/>
        <source>Recently &amp;used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/recentlyusedmenu.cpp" line="41"/>
        <source>&amp;Clear list</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::SearchModel</name>
    <message>
        <location filename="../sources/searchmodel.cpp" line="154"/>
        <source>&lt;b&gt;%1&lt;/b&gt; occurrences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/searchmodel.cpp" line="198"/>
        <source>&lt;b&gt;%1&lt;/b&gt; occurrences on page &lt;b&gt;%2&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::SearchableMenu</name>
    <message>
        <location filename="../sources/miscellaneous.cpp" line="210"/>
        <source>Search for &apos;%1&apos;...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::SettingsDialog</name>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="114"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="147"/>
        <source>&amp;Behavior</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="148"/>
        <source>&amp;Graphics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="149"/>
        <source>&amp;Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="150"/>
        <source>&amp;Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="151"/>
        <source>&amp;Modifiers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="157"/>
        <source>Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="160"/>
        <source>Defaults on current tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="166"/>
        <source>Mouse wheel modifiers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="169"/>
        <source>Mouse button modifiers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="250"/>
        <source>Open URL:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="254"/>
        <source>Auto-refresh:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="258"/>
        <location filename="../sources/settingsdialog.cpp" line="261"/>
        <location filename="../sources/settingsdialog.cpp" line="554"/>
        <location filename="../sources/settingsdialog.cpp" line="585"/>
        <location filename="../sources/settingsdialog.cpp" line="588"/>
        <location filename="../sources/settingsdialog.cpp" line="592"/>
        <location filename="../sources/settingsdialog.cpp" line="595"/>
        <location filename="../sources/settingsdialog.cpp" line="598"/>
        <location filename="../sources/settingsdialog.cpp" line="607"/>
        <source>Effective after restart.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="258"/>
        <source>Track recently used:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="261"/>
        <source>Keep recently closed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="265"/>
        <source>Restore tabs:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="268"/>
        <source>Restore bookmarks:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="271"/>
        <source>Restore per-file settings:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="274"/>
        <source> min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="274"/>
        <source>Save database interval:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="297"/>
        <source>Synchronize presentation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="300"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="300"/>
        <source>Presentation screen:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="307"/>
        <source>Synchronize split views:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="311"/>
        <source>Relative jumps:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="317"/>
        <source>Zoom factor:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="320"/>
        <source>Parallel search execution:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="324"/>
        <source> ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="324"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="324"/>
        <source>Highlight duration:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="327"/>
        <source>Highlight color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="330"/>
        <source>Annotation color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="334"/>
        <source>&apos;%1&apos; is replaced by the absolute file path. &apos;%2&apos; resp. &apos;%3&apos; is replaced by line resp. column number.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="334"/>
        <source>Source editor:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="404"/>
        <source>Use tiling:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="407"/>
        <source>Keep obsolete pixmaps:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="410"/>
        <source>Use device pixel ratio:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="419"/>
        <source>Use logical DPI:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="423"/>
        <source>Decorate pages:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="426"/>
        <source>Decorate links:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="429"/>
        <source>Decorate form fields:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="433"/>
        <source>Background color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="436"/>
        <source>Paper color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="439"/>
        <source>Presentation background color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="443"/>
        <source>Pages per row:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="447"/>
        <location filename="../sources/settingsdialog.cpp" line="450"/>
        <location filename="../sources/settingsdialog.cpp" line="454"/>
        <source> px</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="447"/>
        <source>Page spacing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="450"/>
        <source>Thumbnail spacing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="454"/>
        <source>Thumbnail size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="454"/>
        <source>Fit to viewport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="601"/>
        <source>Document context menu:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="604"/>
        <source>Tab context menu:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="737"/>
        <source>Open in source editor:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="848"/>
        <location filename="../sources/settingsdialog.cpp" line="857"/>
        <source>%1 MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="458"/>
        <source>Cache size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="314"/>
        <source>Minimal scrolling:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="461"/>
        <source>Prefetch:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="464"/>
        <source>Prefetch distance:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="565"/>
        <source>Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="565"/>
        <source>Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="565"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="565"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="564"/>
        <source>Tab position:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="570"/>
        <source>As needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="570"/>
        <source>Always</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="581"/>
        <source>Exit after last tab:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="607"/>
        <source>Scrollable menus:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="610"/>
        <source>Searchable menus:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="734"/>
        <source>Zoom to selection:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="274"/>
        <location filename="../sources/settingsdialog.cpp" line="570"/>
        <source>Never</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="111"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="569"/>
        <source>Tab visibility:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="574"/>
        <source>Spread tabs:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="578"/>
        <source>New tab next to current tab:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="585"/>
        <source>Recently used count:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="588"/>
        <source>Recently closed count:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="554"/>
        <source>Extended search dock:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="614"/>
        <source>Toggle tool and menu bars with fullscreen:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="592"/>
        <source>File tool bar:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="595"/>
        <source>Edit tool bar:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="598"/>
        <source>View tool bar:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="618"/>
        <source>Use page label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="621"/>
        <source>Document title as tab title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="625"/>
        <source>Current page in window title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="628"/>
        <source>Instance name in window title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="304"/>
        <source>Synchronize outline view:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="632"/>
        <source>Highlight current thumbnail:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="635"/>
        <source>Limit thumbnails to results:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="557"/>
        <source>Annotation overlay:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="560"/>
        <source>Form field overlay:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="719"/>
        <source>Zoom:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="722"/>
        <source>Rotate:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="725"/>
        <source>Scroll:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="728"/>
        <source>Copy to clipboard:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="731"/>
        <source>Add annotation:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::ShortcutHandler</name>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="147"/>
        <source>Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="149"/>
        <source>Key sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="362"/>
        <source>Skip backward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="366"/>
        <source>Skip forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="370"/>
        <source>Move up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="374"/>
        <source>Move down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="378"/>
        <source>Move left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="382"/>
        <source>Move right</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::TreeView</name>
    <message>
        <location filename="../sources/miscellaneous.cpp" line="710"/>
        <source>&amp;Expand all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/miscellaneous.cpp" line="711"/>
        <source>&amp;Collapse all</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
