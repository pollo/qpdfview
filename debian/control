Source: qpdfview
Section: graphics
Priority: optional
Maintainer: Louis-Philippe Véronneau <pollo@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 debhelper (>= 13.11.9~),
 libcups2-dev,
 libdjvulibre-dev,
 libjbig2dec0-dev,
 libopenjp2-7-dev,
 libpoppler-qt6-dev,
 libqt6svg6-dev,
 libspectre-dev,
 libsynctex-dev,
 pkgconf,
 qmake6,
 qmake6:native,
 qt6-base-dev,
 qt6-tools-dev-tools,
 zlib1g-dev,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://launchpad.net/qpdfview
Vcs-Git: https://salsa.debian.org/pollo/qpdfview.git
Vcs-Browser: https://salsa.debian.org/pollo/qpdfview

Package: qpdfview
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 hicolor-icon-theme,
 libqt6sql6-sqlite,
 libqt6svg6,
 qpdfview-pdf-poppler-plugin,
Recommends:
 qpdfview-djvu-plugin (= ${binary:Version}),
 qpdfview-ps-plugin (=${binary:Version}),
 qpdfview-translations (=${source:Version}),
Provides: pdf-viewer,
Description: tabbed document viewer
 qpdfview is a simple tabbed document viewer which uses the Poppler library for
 PDF rendering and CUPS for printing and provides a clear and simple Qt
 graphical user interface. Support for the DjVu and PostScript formats can be
 added via plugins.
 .
 Current features include:
   - Outline, properties and thumbnail panes
   - Scale, rotate and fit
   - Fullscreen and presentation views
   - Continuous and multi-page layouts
   - Search for text (PDF and DjVu only)
   - Configurable toolbars
   - SyncTeX support (PDF only)
   - Partial annotation support (PDF only, Poppler version 0.20.1 or newer)
   - Partial form support (PDF only)
   - Persistent per-file settings
   - Support for DjVu and PostScript documents via plugins

Package: qpdfview-pdf-poppler-plugin
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Replaces:
 qpdfview (<< 0.4.18-3~),
Breaks: qpdfview (<< 0.4.18-3~)
Description: tabbed document viewer - Poppler plugin
 qpdfview is a simple tabbed document viewer which uses the Poppler library for
 PDF rendering and CUPS for printing and provides a clear and simple Qt
 graphical user interface. Support for the DjVu and PostScript formats can be
 added via plugins.
 .
 This plugin adds support for the PDF format based on Poppler library.

Package: qpdfview-djvu-plugin
Architecture: any
Depends:
 qpdfview (=${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 djvu-viewer,
Description: tabbed document viewer - DjVu plugin
 qpdfview is a simple tabbed document viewer which uses the Poppler library for
 PDF rendering and CUPS for printing and provides a clear and simple Qt
 graphical user interface. Support for the DjVu and PostScript formats can be
 added via plugins.
 .
 This plugin adds support for the DjVu format.

Package: qpdfview-ps-plugin
Architecture: any
Depends:
 qpdfview (=${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 postscript-viewer,
Description: tabbed document viewer - PostScript plugin
 qpdfview is a simple tabbed document viewer which uses the Poppler library for
 PDF rendering and CUPS for printing and provides a clear and simple Qt
 graphical user interface. Support for the DjVu and PostScript formats can be
 added via plugins.
 .
 This plugin adds support for the PostScript format.

Package: qpdfview-translations
Architecture: all
Depends:
 qpdfview (>=${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Replaces:
 qpdfview-languages,
Description: tabbed document viewer - translations
 qpdfview is a simple tabbed document viewer which uses the Poppler library for
 PDF rendering and CUPS for printing and provides a clear and simple Qt
 graphical user interface. Support for the DjVu and PostScript formats can be
 added via plugins.
 .
 This package contains all translations.
